class CreateSateAuthUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :sate_auth_users do |t|
      t.string :first_name, limit: 50, null: false
      t.string :last_name, limit: 50, null: false
      t.string :email, limit: 50, null: false
      t.string :password_digest, null: false
      t.boolean :active, null: false, default: true
      t.references :application_module, index: true

      t.timestamps
    end

    add_index :sate_auth_users, :email, unique: true
    add_foreign_key :sate_auth_users, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
