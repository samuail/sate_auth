class CreateSateAuthApplicationModules < ActiveRecord::Migration[5.2]
  def change
    create_table :sate_auth_application_modules do |t|
      t.string :code, unique: true, limit: 4, null: false
      t.string :name, null: false

      t.timestamps
    end
    add_index :sate_auth_application_modules, :code, :unique =>  true
  end
end
