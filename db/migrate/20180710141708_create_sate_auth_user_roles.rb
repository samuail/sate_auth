class CreateSateAuthUserRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :sate_auth_user_roles do |t|
      t.string :name, limit: 50, null: false
      t.references :application_module, index: true

      t.timestamps
    end

    add_index :sate_auth_user_roles, [:name, :application_module_id], :unique => true

    add_foreign_key :sate_auth_user_roles, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
