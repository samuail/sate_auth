@application_module
Feature: Capturing application module Information.
  As a system user
  I want to capture application module information
  So that the application module can easily be identified

  Background:
    Given I am logged in as a super user

  @create
  Scenario: Adding the application module information
    When I record a application module by the name "Budget & Finance", and "BF" as code
    Then I should have this application module information
      | application_name | code |
      | Budget & Finance |  BF  |
    And I should see a success message "Application module was successfully created."

  @create_with_null_error_message
  Scenario: Adding the application module information
    When I record a application module by the name "Budget & Finance", and "" as code
    Then I should see an error message "Application Module Code can't be blank"

  @create_with_duplicate_error_message
  Scenario: Adding the application module information
    And I have the following application module information
      | application_name | code |
      | Budget & Finance |  BF  |
    When I record a application module by the name "Budget & Finance", and "BF" as code
    Then I should see an error message "Application Module Code has already been taken"

  @update
  Scenario: Editing the application module information
    And I have the following application module information
      | application_name | code |
      | Budget & Finance |  BF  |
    When I change the application module by the name "Budget & Finance", and "BFI" as code
    Then I should have this application module information
      | application_name | code |
      | Budget & Finance | BFI  |
    And I should see a success message "Application module was successfully updated."

  @update_with_null_error_message
  Scenario: Editing the application module information
    And I have the following application module information
      | application_name | code |
      | Budget & Finance |  BF  |
    When I change the application module by the name "Budget & Finance", and "" as code
    Then I should see an error message "Application Module Code can't be blank"

  @update_with_duplicate_error_message
  Scenario: Editing the application module information
    And I have the following application module information
      | application_name | code |
      | Budget & Finance | BFI  |
    And I have the following application module information
      | application_name | code |
      | Budget & Finance |  BF  |
    When I change the application module by the name "Budget & Finance", and "BFI" as code
    Then I should see an error message "Application Module Code has already been taken"

  @list_all_application_modules
  Scenario: List all application modules information
    And I have the following application module information
      |         application_name      | code |
      | Budget & Finance Information  | BFI  |
    And I have the following application module information
      | application_name | code |
      | Budget & Finance |  BF  |
    When I want to see all applicaion module information
    Then I should have the following "2" "application module" informations
      |         application_name      | code |
      | Budget & Finance Information  | BFI  |
      | Budget & Finance              |  BF  |