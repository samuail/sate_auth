require 'simplecov'
SimpleCov.start

ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../../spec/dummy/config/environment", __FILE__)
ENV["RAILS_ROOT"] ||= File.dirname(__FILE__) + '../../../spec/dummy'

require 'cucumber/rails'
require 'factory_bot_rails'

ActionController::Base.allow_rescue = false

begin
  DatabaseCleaner.strategy = :transaction
rescue NameError
  raise "You need to add database_cleaner to your Gemfile (in the :test group) if you wish to use it."
end

Cucumber::Rails::Database.javascript_strategy = :truncation