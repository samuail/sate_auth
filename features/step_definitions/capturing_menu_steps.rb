When("I record a menu information with {string} as text {string} as icon {string} as class name {string} as location and {string} as parent to be used on {string} application module") do
|text, icon, class_name, location, parent, app_module|
  menus_path = sate_auth_path + "/menus"

  @response = post menus_path, :menu => { 'text' => text,
                                          'icon_cls' => icon,
                                          'class_name' => class_name,
                                          'location' => location,
                                          'parent_id' => nil }
end

Then("I should have this menu information") do |menu|
  @response = JSON(@response.body)
  expect(@response["data"]["text"]).to eq menu.raw[1][0]
  expect(@response["data"]["icon_cls"]).to eq menu.raw[1][1]
  expect(@response["data"]["class_name"]).to eq menu.raw[1][2]
  expect(@response["data"]["location"]).to eq menu.raw[1][3]
  expect(@response["data"]["parent_id"]).to eq nil
end

Given("I have the following menu information") do |menu_item|
  @menu = Sate::Auth::Menu.create text: menu_item.raw[1][0],
                                  icon_cls: menu_item.raw[1][1],
                                  class_name: menu_item.raw[1][2],
                                  location: menu_item.raw[1][3],
                                  parent_id: nil,
                                  application_module_id: @appli_module.id
end

When("I change the menu information with {string} as text {string} as icon {string} as class name {string} as location and {string} as parent to be used on {string} application module") do
|text, icon, class_name, location, parent, app_name|
  menus_path = sate_auth_path + "/menus/" + @menu.id.to_s
  @response = put menus_path, :menu => { 'text' => text,
                                          'icon_cls' => @menu.icon_cls,
                                          'class_name' => @menu.class_name,
                                          'location' => @menu.location,
                                          'parent_id' => @menu.parent_id }
end

When("I want to see all menu information") do
  menus_path = sate_auth_path + "/menus"
  @response = get menus_path
end

Then("I should have the following {string} menu informations") do |num, menu_items|
  @response = JSON(@response.body)

  expect(@response["total"].to_s).to eq num

  expect(@response["data"][0]["text"]).to eq menu_items.raw[1][0]

  expect(@response["data"][1]["text"]).to eq menu_items.raw[2][0]
end