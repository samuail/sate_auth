#  Create & Update User

When("I record a user by the first name {string}, {string} as a last name, {string} as contact email address, and {string} as password to work on {string} application module") do |
first_name, last_name, email, password, app_mod|

  users_path = sate_auth_path + "/users"
  @response = post users_path, :user => { 'first_name' => first_name,
                                          'last_name' => last_name,
                                          'email' => email,
                                          'password' => password }
end

Then("I should have this user information") do |user|

  @response = JSON(@response.body)
  expect(@response["data"]["first_name"]).to eq user.raw[1][0]
  expect(@response["data"]["last_name"]).to eq user.raw[1][1]
  expect(@response["data"]["email"]).to eq user.raw[1][2]

end

Then("I should see an error message {string} and {string}") do |message1, message2|
  @response = JSON(@response.body)
  message = []
  message.push(message1)
  message.push(message2)
  expect(@response["errors"]).to eq message
end

Given("I have the following user information") do |user|
  @user = Sate::Auth::User.create first_name: user.raw[1][0],
                                  last_name: user.raw[1][1],
                                  email: user.raw[1][2],
                                  password: user.raw[1][3],
                                  application_module_id: @appli_module.id
end

When("I change the users contact email address by {string}") do |email|
  users_path = sate_auth_path + "/users/" + @user.id.to_s
  @response = put users_path, :user => { 'first_name' => @user.first_name,
                                         'last_name' => @user.last_name,
                                         'email' => email,
                                         'password' => @user.password_digest }
end

#  List all User Information

When("I want to see all user information") do
  users_path = sate_auth_path + "/users"
  @response = get users_path
end

Then("I should have the following {string} user informations") do |num, users|
  @response = JSON(@response.body)

  expect(@response["total"].to_s).to eq num

  expect(@response["data"][0]["first_name"]).to eq users.raw[1][0]
  expect(@response["data"][0]["last_name"]).to eq users.raw[1][1]
  expect(@response["data"][0]["email"]).to eq users.raw[1][2]

  expect(@response["data"][1]["first_name"]).to eq users.raw[2][0]
  expect(@response["data"][1]["last_name"]).to eq users.raw[2][1]
  expect(@response["data"][1]["email"]).to eq users.raw[2][2]
end
