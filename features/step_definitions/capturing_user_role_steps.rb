When("I record a user role with the name {string} to work on {string} application module") do
|role_name, app_mod|
  user_roles_path = sate_auth_path + "/user_roles"
  @response = post user_roles_path, :user_role => { 'name' => role_name }
end

Then("I should have this user role information") do |user_role|
  @response = JSON(@response.body)
  expect(@response["data"]["name"]).to eq user_role.raw[1][0]
end

Given("I have the following user role information") do |user_role|
  @user_role = Sate::Auth::UserRole.create name: user_role.raw[1][0],
                              application_module_id: @appli_module.id
end

When("I change the user roles name by {string} to work on {string} application module") do
|role_name, app_name|
  app_module = Sate::Auth::ApplicationModule.find_by_name app_name
  user_roles_path = sate_auth_path + "/user_roles/" + @user_role.id.to_s
  @response = put user_roles_path, :user_role => { 'name' => role_name }
end

When("I want to see all user role information") do
  user_roles_path = sate_auth_path + "/user_roles"
  @response = get user_roles_path
end

Then("I should have the following {string} user role informations") do |num, user_roles|
  @response = JSON(@response.body)

  expect(@response["total"].to_s).to eq num

  expect(@response["data"][0]["name"]).to eq user_roles.raw[1][0]

  expect(@response["data"][1]["name"]).to eq user_roles.raw[2][0]
end

