Sate::Auth::Engine.routes.draw do
  resources :application_modules
  resources :users
  resources :user_roles
  resources :menus
end
