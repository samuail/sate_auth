# SateAuth
This gem is common implementation to handle user account creation, role creation, defining application activities as menu items, and authenticating a user based on his/her username and password. This can also be further extended to handle user activity assignment which majorly depends on specific application. 

## Usage
In order to use this gem first we have to put this statement "gem 'sate_auth'" in your project gemfile and install the gem using this statement "gem install sate_auth-0.1.0.gem" in your project directory. Then create the database and run the migration to create application module, user, user role, and menu tables.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'sate_auth'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install sate_auth
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
