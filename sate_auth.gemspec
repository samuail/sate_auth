$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "sate/auth/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "sate_auth"
  s.version     = Sate::Auth::VERSION
  s.authors     = ["Samuel Teshome"]
  s.email       = ["samuailteshome@yahoo.com"]
  s.homepage    = "http://www.sate.com.et/auth"
  s.summary     = "Core authentication module."
  s.description = "This engine contains the core authentication module for future applications."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "bcrypt", "~> 3.1", ">= 3.1.11"
  s.add_dependency "rails", "~> 5.2.0"

  s.add_development_dependency "database_cleaner", "~> 1.6", ">= 1.6.2"
  s.add_development_dependency "factory_bot_rails", "~> 4.8", ">= 4.8.2"
  s.add_development_dependency "ffaker", "~> 2.9"
  s.add_development_dependency "rspec-rails", "~> 3.7", ">= 3.7.2"
  s.add_development_dependency "shoulda-matchers", "~> 3.1", ">= 3.1.2"
  s.add_development_dependency "sqlite3", "~> 1.3", ">= 1.3.13"
end