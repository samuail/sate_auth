require 'rails_helper'

module Sate::Auth
  RSpec.describe ApplicationModulesController, type: :controller do
    routes { Sate::Auth::Engine.routes }

    before :each do
      @app_module = build(:sate_auth_application_module)
    end

    describe "GET #index" do

      before :each do
        @application_module_get =
            Sate::Auth::ApplicationModule.create code: @app_module.code,
                                                 name: @app_module.name
        get :index
      end

      it "returns a success response" do
        expect(response).to be_successful
      end

      it "assigns all application module as @application_modules" do
        expect(assigns(:application_modules)).to eq([@application_module_get])
      end
    end

    describe "POST #create" do
      code = nil
      let!(:code) { code }

      before :each do
        create :sate_auth_application_module, code: "BFI",
               name: @app_module.name
        post :create, params: { application_module: { 'code' => code,
                                                      'name' => @app_module.name } }
        @decoded_response = JSON(@response.body)
      end
      context "with valid params" do

        let!(:code) { code = @app_module.code }

        it "creates a new ApplicationModule" do
          expect(ApplicationModule.count).to eq 2
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Application module was successfully created."
        end
      end

      context "with invalid params" do
        context "Blank Application Module Code" do
          let!(:code){ code = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Application Module Code can't be blank"]
          end
        end

        context "Duplicate Application Module Code" do
          let!(:code) { code = "BFI" }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Application Module Code has already been taken"]
          end
        end
      end
    end

    describe "PUT #update" do
      code = nil
      let!(:code) { code }

      before :each do
        @app_module_update = create(:sate_auth_application_module)
        put :update, params: { id: @app_module_update.to_param,
                               application_module: { 'code' => code,
                                                     'name' => @app_module.name } }
        @app_module_update.reload
        @decoded_response = JSON(@response.body)
      end
      context "with valid params" do

        let!(:code){ code = "BFO" }

        it "updates the requested application module" do
          expect(@app_module_update.code).to_not eq attributes_for(:sate_auth_application_module)[:code]
        end

        it "return a success message" do
          expect(@decoded_response["success"]).to eq true
          expect(@decoded_response["message"]).to eq "Application module was successfully updated."
        end
      end

      context "with invalid params" do

        let!(:code){ code = nil }

        it "returns an error message" do
          expect(@decoded_response["success"]).to eq false
          expect(@decoded_response["errors"]).to eq ["Application Module Code can't be blank"]
        end
      end
    end
  end
end
