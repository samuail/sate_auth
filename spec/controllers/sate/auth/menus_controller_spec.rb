require 'rails_helper'

module Sate::Auth
  RSpec.describe MenusController, type: :controller do
    routes { Sate::Auth::Engine.routes }

    before :each do
      @menu = build(:sate_auth_menu)
      @application_module = Sate::Auth::ApplicationModule.create code: 'BF',
                                                                 name: 'Budget & Finance'
    end
    describe "GET #index" do

      before :each do
        @menu_get = Sate::Auth::Menu.create text: @menu.text,
                                            icon_cls: @menu.icon_cls,
                                            class_name: @menu.class_name,
                                            location: @menu.location,
                                            parent_id: @menu.parent_id,
                                            application_module_id: @application_module.id
        get :index
      end

      it "returns a success response" do
        expect(response).to be_successful
      end

      it "assigns all menus as @menus" do
        expect(assigns(:menus)).to eq([@menu_get])
      end
    end

    describe "POST #create" do
      text = nil
      let!(:text) { text }

      before :each do
        create :sate_auth_menu, text: "System Setting",
               icon_cls: @menu.icon_cls,
               class_name: @menu.class_name,
               location: @menu.location,
               parent_id: @menu.parent_id,
               application_module_id: @application_module.id
        post :create, params: { menu: { 'text' => text,
                                        'icon_cls' => @menu.icon_cls,
                                        'class_name' => @menu.class_name,
                                        'location' => @menu.location,
                                        'parent_id' => @menu.parent_id } }
        @decoded_response = JSON(@response.body)
      end
      context "with valid params" do
        let!(:text) { text = @menu.text }


        it "creates a new Menu" do
          expect(Menu.count).to eq 2
        end

        it "returns a success message" do
          expect(@response).to be_successful
          expect(@decoded_response["message"]).to eq "Menu was successfully created."
        end
      end

      context "with invalid params" do
        context "Blank Menu Text" do
          let!(:text){ text = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Menu Text can't be blank"]
          end
        end

        context "Duplicate Menu Text" do
          let!(:text) { text = "System Setting" }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Menu Text has already been taken"]
          end
        end
      end
    end

    describe "PUT #update" do
      text = nil
      let!(:text) { text }

      before :each do
        @menu_update = create :sate_auth_menu, text: "System Setting",
               icon_cls: @menu.icon_cls,
               class_name: @menu.class_name,
               location: @menu.location,
               parent_id: @menu.parent_id,
               application_module_id: @application_module.id
        create :sate_auth_menu, text: "Users",
               icon_cls: @menu.icon_cls,
               class_name: @menu.class_name,
               location: @menu.location,
               parent_id: @menu.parent_id,
               application_module_id: @application_module.id
        put :update, params: { id: @menu_update.to_param,
                               menu: { 'text' => text,
                                       'icon_cls' => @menu.icon_cls,
                                       'class_name' => @menu.class_name,
                                       'location' => @menu.location,
                                       'parent_id' => @menu.parent_id } }
        @menu_update.reload
        @decoded_response = JSON(@response.body)
      end
      context "with valid params" do
        let!(:text){ text = "Business Setting" }

        it "updates the requested menu" do
          expect(@menu_update.text).to_not eq attributes_for(:sate_auth_menu)[:text]
        end

        it "return a success message" do
          expect(@decoded_response["success"]).to eq true
          expect(@decoded_response["message"]).to eq "Menu was successfully updated."
        end
      end

      context "with invalid params" do
        context "Blank Menu Text" do
          let!(:text){ text = nil }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Menu Text can't be blank"]
          end
        end

        context "Duplicate Menu Text" do
          let!(:text) { text = "Users" }

          it "returns an error message" do
            expect(@decoded_response["success"]).to eq false
            expect(@decoded_response["errors"]).to eq ["Menu Text has already been taken"]
          end
        end
      end
    end

  end
end
