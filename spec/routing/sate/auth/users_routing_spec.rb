require "rails_helper"

module Sate::Auth
  RSpec.describe UsersController, type: :routing do
    describe "routing" do

      routes { Sate::Auth::Engine.routes }

      it "routes to #index" do
        expect(:get => "/users").to route_to("sate/auth/users#index")
      end

      it "routes to #create" do
        expect(:post => "/users").to route_to("sate/auth/users#create")
      end

      it "routes to #update via PUT" do
        expect(:put => "/users/1").to route_to("sate/auth/users#update", :id => "1")
      end

      it "routes to #update via PATCH" do
        expect(:patch => "/users/1").to route_to("sate/auth/users#update", :id => "1")
      end

    end
  end
end
