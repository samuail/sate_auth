require "rails_helper"

module Sate::Auth
  RSpec.describe ApplicationModulesController, type: :routing do
    describe "routing" do

      routes { Sate::Auth::Engine.routes }

      it "routes to #index" do
        expect(:get => "/application_modules").to route_to("sate/auth/application_modules#index")
      end

      it "routes to #create" do
        expect(:post => "/application_modules").to route_to("sate/auth/application_modules#create")
      end

      it "routes to #update via PUT" do
        expect(:put => "/application_modules/1").to route_to("sate/auth/application_modules#update", :id => "1")
      end

      it "routes to #update via PATCH" do
        expect(:patch => "/application_modules/1").to route_to("sate/auth/application_modules#update", :id => "1")
      end

    end
  end
end