FactoryBot.define do
  factory :sate_auth_application_module, class: 'Sate::Auth::ApplicationModule' do
    code { FFaker::Name.name[0..3] }
    name { FFaker::Name.name }
  end
end