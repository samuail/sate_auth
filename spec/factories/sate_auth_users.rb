FactoryBot.define do
  factory :sate_auth_user, class: 'Sate::Auth::User' do
    first_name { FFaker::Name.name[0..49] }
    last_name { FFaker::Name.name[0..49] }
    email { FFaker::Internet.email[0..49] }
    password { FFaker::Internet.password(min_length=6) }
    active true
    application_module {create (:sate_auth_application_module) }
  end
end
