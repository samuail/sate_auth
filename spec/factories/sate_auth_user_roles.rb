FactoryBot.define do
  factory :sate_auth_user_role, class: 'Sate::Auth::UserRole' do
    name { FFaker::Name.name[0..49] }
    application_module {create (:sate_auth_application_module) }
  end
end
