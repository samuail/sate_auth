FactoryBot.define do
  factory :sate_auth_menu, class: 'Sate::Auth::Menu' do
    text { FFaker::Name.name[0..49] }
    icon_cls { FFaker::Name.name }
    class_name { FFaker::Name.name }
    location { FFaker::Name.name }
    parent_id nil
    application_module {create (:sate_auth_application_module) }
  end
end
