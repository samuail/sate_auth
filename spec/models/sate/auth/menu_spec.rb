require 'rails_helper'

module Sate::Auth
  RSpec.describe Menu, type: :model do
    context "db" do
      context "columns" do
        it { should have_db_column(:text).of_type(:string).with_options(limit: 50, null: false) }
        it { should have_db_column(:icon_cls).of_type(:string) }
        it { should have_db_column(:class_name).of_type(:string) }
        it { should have_db_column(:location).of_type(:string) }
        it { should have_db_column(:parent_id).of_type(:integer) }
        it { should have_db_column(:application_module_id).of_type(:integer) }
      end
    end
    context "model" do
      context "factories" do
        it { expect(build(:sate_auth_menu)).to be_valid }
      end
      context "validations" do
        subject { build(:sate_auth_menu) }
        it { should validate_presence_of :text }
        it { should validate_uniqueness_of(:text).
            scoped_to(:application_module_id).
            case_insensitive
        }
      end
      context "associations" do
        it { should belong_to(:application_module).
            class_name('Sate::Auth::ApplicationModule') }
        it { should belong_to(:parent).class_name('Sate::Auth::Menu') }
        it { should have_many(:children).class_name('Sate::Auth::Menu').
            with_foreign_key('parent_id') }
      end
    end
  end
end
