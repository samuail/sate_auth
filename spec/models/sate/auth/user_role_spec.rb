require 'rails_helper'

module Sate::Auth
  RSpec.describe UserRole, type: :model do
    context "db" do
      context "columns" do
        it { should have_db_column(:name).of_type(:string).with_options(limit: 50, null: false) }
        it { should have_db_column(:application_module_id).of_type(:integer) }
      end
    end
    context "model" do
      context "factories" do
        it { expect(build(:sate_auth_user_role)).to be_valid }
      end
      context "validations" do
        subject { build(:sate_auth_user_role) }
        it { should validate_presence_of :name }
        it { should validate_uniqueness_of(:name).
            scoped_to(:application_module_id).
            case_insensitive
        }
      end
      context "associations" do
        it { should belong_to(:application_module).
            class_name('Sate::Auth::ApplicationModule') }
      end
    end
  end
end
