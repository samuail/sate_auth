require 'rails_helper'

module Sate::Auth
  RSpec.describe ApplicationModule, type: :model do
    context "db" do
      context "indexes" do
        it { should have_db_index(:code).unique(true) }
      end

      context "columns" do
        it { should have_db_column(:code).of_type(:string).with_options(null: false, limit: 4) }
        it { should have_db_column(:name).of_type(:string).with_options(null: false) }
      end
    end

    context "model" do
      context "factories" do
        it { expect(build(:sate_auth_application_module)).to be_valid }
      end

      context "validations" do
        subject { build(:sate_auth_application_module) }
        it { should validate_presence_of :code }
        it { should validate_presence_of :name }
        it { should validate_uniqueness_of(:code) }
        it { should validate_length_of(:code).is_at_most(4) }
        it "should uppcase code before saving" do
          app_module = build_stubbed(:sate_auth_application_module)
          a_module = Sate::Auth::ApplicationModule.create code: app_module.code,
                                                          name: app_module.name
          expect(a_module.code).not_to eq app_module.code
        end
      end

      context "associations" do
        it { should have_many(:users).class_name('Sate::Auth::User') }
        it { should have_many(:user_roles).class_name('Sate::Auth::UserRole') }
        it { should have_many(:menus).class_name('Sate::Auth::Menu') }
      end

    end
  end
end