require 'rails_helper'

module Sate::Auth
  RSpec.describe User, type: :model do
    context "db" do
      context "indexes" do
        it { should have_db_index(:email).unique(true) }
      end

      context "columns" do
        it { should have_db_column(:email).of_type(:string).with_options(limit: 50, null: false) }
        it { should have_db_column(:first_name).of_type(:string).with_options(limit: 50, null: false) }
        it { should have_db_column(:last_name).of_type(:string).with_options(limit: 50, null: false) }
        it { should have_db_column(:active).of_type(:boolean).with_options(default: true, null: false) }
        it { should have_db_column(:password_digest).of_type(:string).with_options(null: false) }
        it { should have_db_column(:application_module_id).of_type(:integer) }
      end
    end

    context "model" do
      context "factories" do
        it { expect(build(:sate_auth_user)).to be_valid }
      end

      context "validations" do
        subject { build(:sate_auth_user) }
        it { should validate_presence_of :first_name }
        it { should validate_presence_of :last_name }
        it { should validate_presence_of :email }
        it { should validate_presence_of :password_digest }
        it { should validate_uniqueness_of(:email).case_insensitive }
        it { should allow_value('first.last@example.com').for(:email) }
        it { should_not allow_value('first.lastexample.com', 'first.last@example',
                                    'first.last@example.', '@example.com').for(:email)}
        it { should have_secure_password }
        it "should downcase email before saving" do
          user_stub = build_stubbed(:sate_auth_user)
          user = Sate::Auth::User.create first_name: user_stub.first_name,
                                         last_name: user_stub.last_name,
                                         email: user_stub.email.upcase,
                                         password: user_stub.password,
                                         application_module_id: user_stub.application_module_id
          expect(user.email).not_to eq user_stub.email
        end
      end

      context "built in functions" do
        it "should return full name" do
          user_stub = build_stubbed(:sate_auth_user)
          user = Sate::Auth::User.create first_name: user_stub.first_name,
                                         last_name: user_stub.last_name,
                                         email: user_stub.email.upcase,
                                         password: user_stub.password,
                                         application_module_id: user_stub.application_module_id
          expect(user.full_name).to eq user_stub.first_name + " " + user_stub.last_name
        end
      end
      context "associations" do
        it { should belong_to(:application_module).
            class_name('Sate::Auth::ApplicationModule') }
      end
    end
  end
end
