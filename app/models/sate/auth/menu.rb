module Sate::Auth
  class Menu < ApplicationRecord

    belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'
    belongs_to :parent, class_name: 'Sate::Auth::Menu', optional: true
    has_many :children, class_name: 'Sate::Auth::Menu', foreign_key: 'parent_id'

    validates :text,
              presence: true,
              uniqueness: {
                  scope: [:application_module_id],
                  case_sensitive: false
              }

  end
end
