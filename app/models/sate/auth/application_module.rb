module Sate::Auth
  class ApplicationModule < ApplicationRecord
    validates :code, presence: true, uniqueness: true, length: { maximum: 4 }
    validates :name, presence: true

    before_save { code.upcase! }

    has_many :users, class_name: 'Sate::Auth::User'
    has_many :user_roles, class_name: 'Sate::Auth::UserRole'
    has_many :menus, class_name: 'Sate::Auth::Menu'
  end
end
