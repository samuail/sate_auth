module Sate::Auth
  class User < ApplicationRecord
    has_secure_password

    belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

    validates :first_name, :last_name, :password_digest, presence: true

    VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
    validates :email, presence: true,
              uniqueness: { case_sensitive: false },
              format: { with: VALID_EMAIL_REGEX }

    before_save { email.downcase }

    def full_name
      "#{first_name} #{last_name}"
    end
  end
end
