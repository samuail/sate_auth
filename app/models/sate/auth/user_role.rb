module Sate::Auth
  class UserRole < ApplicationRecord

    belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'

    validates :name, presence: true

    validates :name,
              uniqueness: {
                  scope: [:application_module_id],
                  case_sensitive: false
              }

  end
end
