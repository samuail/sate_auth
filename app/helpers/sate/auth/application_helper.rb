module Sate
  module Auth
    module ApplicationHelper
      def app_module
        code = Rails.configuration.x.application_module_code
        ApplicationModule.find_by(:code => code)
      end
    end
  end
end
