require_dependency "sate/auth/application_controller"

module Sate::Auth
  class MenusController < ApplicationController
    before_action :set_menu, only: [:update]

    # GET /menus
    def index
      @menus = Menu.where(application_module_id: app_module.id)
      total = @menus.count
      response = Sate::Common::MethodResponse.new(true, nil, @menus, nil, total)
      render json: response
    end

    # POST /menus
    def create
      @menu = Menu.new(menu_params)
      @menu.application_module_id = app_module.id

      if @menu.save
        response = Sate::Common::MethodResponse.new(
            true, 'Menu was successfully created.', @menu, nil, 0)
      else
        errors = Sate::Common::Util.error_messages @menu, 'Menu'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
      render json: response
    end

    # PATCH/PUT /menus/1
    def update
      if @menu.update(menu_params)
        response = Sate::Common::MethodResponse.new(
            true, 'Menu was successfully updated.', @menu, nil, 0)
      else
        errors = Sate::Common::Util.error_messages @menu, 'Menu'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
      render json: response
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu
      @menu = Menu.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def menu_params
      params.require(:menu).permit(:text, :icon_cls, :class_name,
                                   :location, :parent_id)
    end
  end
end
