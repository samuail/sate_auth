require 'sate/common/methodresponse'
require 'sate/common/util'
module Sate
  module Auth
    class ApplicationController < ::ApplicationController
      include ApplicationHelper
    end
  end
end
