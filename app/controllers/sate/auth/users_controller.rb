require_dependency "sate/auth/application_controller"

module Sate::Auth
  class UsersController < ApplicationController
    before_action :set_user, only: [:update]

    # GET /users
    def index
      @users = User.where(application_module_id: app_module.id)
      total = @users.count
      response = Sate::Common::MethodResponse.new(true, nil, @users, nil, total)
      render json: response
    end


    # POST /users
    def create
      @user = User.new(user_params)
      @user.application_module_id = app_module.id

      if @user.save
        response = Sate::Common::MethodResponse.new(
            true, 'User was successfully created.', @user, nil, 0)
      else
        errors = Sate::Common::Util.error_messages @user, 'User'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
      render json: response
    end

    # PATCH/PUT /users/1
    def update
      if @user.update(user_params)
        response = Sate::Common::MethodResponse.new(
            true, 'User was successfully updated.', @user, nil, 0)
      else
        errors = Sate::Common::Util.error_messages @user, 'User'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
      render json: response
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password)
    end
  end
end