require_dependency "sate/auth/application_controller"

module Sate::Auth
  class UserRolesController < ApplicationController
    before_action :set_user_role, only: [:update]

    # GET /user_roles
    def index
      @user_roles = UserRole.where(application_module_id: app_module.id)
      total = @user_roles.count
      response = Sate::Common::MethodResponse.new(true, nil, @user_roles, nil, total)
      render json: response
    end


    # POST /user_roles
    def create
      @user_role = UserRole.new(user_role_params)
      @user_role.application_module_id = app_module.id

      if @user_role.save
        response = Sate::Common::MethodResponse.new(
            true, 'User role was successfully created.', @user_role, nil, 0)
      else
        errors = Sate::Common::Util.error_messages @user_role, 'User Role'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
      render json: response
    end

    # PATCH/PUT /user_roles/1
    def update
      if @user_role.update(user_role_params)
        response = Sate::Common::MethodResponse.new(
            true, 'User role was successfully updated.', @user_role, nil, 0)
      else
        errors = Sate::Common::Util.error_messages @user_role, 'User Role'
        response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
      end
      render json: response
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_role
      @user_role = UserRole.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_role_params
      params.require(:user_role).permit(:name)
    end
  end
end
